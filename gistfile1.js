var Promise = require("bluebird");

function promisifyWaterline(action){
	return Promise.try(function() {
		return action();
	}).catch(function(err) {
		// you should almost never catch errors inline. ONLY do it if you need to 'repackage' an error.
		// definitely don't do the below, as you lose the original error.
		throw new Error("lol");
	})
}
